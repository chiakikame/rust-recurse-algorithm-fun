#![feature(test)]

extern crate test;
extern crate rust_recurse;

use test::Bencher;
use rust_recurse::*;

const N: u32 = 10;
const MULTI_LOOP_COUNT: u32 = 100;

#[bench]
fn bench_loop(b: &mut Bencher) {
    b.iter(|| {
        simple_loop(N);
    });
}

#[bench]
fn bench_dynamic_programming(b: &mut Bencher) {
    b.iter(|| {
        dynamic_programming(N);
    });
}

#[bench]
fn bench_simple_very_naive(b: &mut Bencher) {
    b.iter(|| {
        simple_very_naive(N);
    });
}

#[bench]
fn bench_simple_naive(b: &mut Bencher) {
    b.iter(|| {
        simple_naive(N);
    });
}

#[bench]
fn bench_loop_multi(b: &mut Bencher) {
    b.iter(|| {
        let mut result = simple_loop(N);
        for _ in 0 .. MULTI_LOOP_COUNT {
            result = simple_loop(N);
        }
    });
}

#[bench]
fn bench_dynamic_programming_multi(b: &mut Bencher) {
    b.iter(|| {
        let mut result = dynamic_programming(N);
        for _ in 0 .. MULTI_LOOP_COUNT {
            result = dynamic_programming_with_cache(N, result.get_cache());
        }
    });
}

#[bench]
fn bench_simple_very_naive_multi(b: &mut Bencher) {
    b.iter(|| {
        let mut result = simple_very_naive(N);
        for _ in 0 .. MULTI_LOOP_COUNT {
            result = simple_very_naive(N);
        }
    });
}

#[bench]
fn bench_simple_naive_multi(b: &mut Bencher) {
    b.iter(|| {
        let mut result = simple_naive(N);
        for _ in 0 .. MULTI_LOOP_COUNT {
            result = simple_naive(N);
        }
    });
}
