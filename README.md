# rust-recurse

A recursive implementation of the following problem:

```
A formula looks like:
f(0) -> "0"
f(n) -> "1" + f(n-1) + f(n-1)

e.g.
f(0) -> 0
f(1) -> 100
f(2) -> 1100100
f(3) -> 111001001100100
```

The problem itself is not easy but contains some interesting aspects:

* The requirement (math formula) is defined in recursive way
* It may utilize dynamic programming to accelerate some of the calculation
* It may implemented with a loop

This code set contains unit tests and benchmarks (which only available with rust nightly).

## Usage

To run tests

```
$ cargo test
```

To run benchmarks

```
$ cargo bench
```

Beware that benchmarks are only available in nightly rust for now.
