use fillers::result_interface::RecursiveResult;

pub struct DynamicProgrammingResult {
    n: u32,
    content: String,
    call_count: u32,
    cache: Vec<String>
}

pub fn dynamic_programming(n: u32) -> DynamicProgrammingResult {
    let mut state = DPState::new();
    let result = state.run(n);
    
    DynamicProgrammingResult {
        n: n,
        content: result,
        call_count: state.call_count,
        cache: state.cache
    }
}

pub fn dynamic_programming_with_cache(n: u32, cache: Vec<String>) -> DynamicProgrammingResult {
    let mut state = DPState::from_cache(cache);
    let result = state.run(n);
    
    DynamicProgrammingResult {
        n: n,
        content: result,
        call_count: state.call_count,
        cache: state.cache
    }
}

impl RecursiveResult for DynamicProgrammingResult {
    fn get_n(&self) -> u32 { self.n }
    fn get_content(&self) -> String { self.content.clone() }
    fn get_call_count(&self) -> u32 { self.call_count }
}

impl DynamicProgrammingResult {
    pub fn get_cache(&self) -> Vec<String> { self.cache.clone() }
}

struct DPState {
    cache: Vec<String>,
    call_count: u32
}

impl DPState {
    fn new() -> Self {
        DPState {
            cache: vec!["0".to_string()],
            call_count: 0
        }
    }
    
    fn from_cache(cache: Vec<String>) -> Self {
        DPState {
            cache: cache,
            call_count: 0
        }
    }
    
    fn run(&mut self, n: u32) -> String {
        if (n as usize) < self.cache.len() {
            // request item is in cache
            self.cache[n as usize].clone()
        } else {
            // do actual calculation
            self.call_count += 1;
            let prev_result = self.run(n - 1);
            let result = "1".to_string() + &prev_result + &prev_result;
            // since self.run(n - 1) is finished
            // self.cache[n - 1] exists
            // and self.cache[n] does not exist
            self.cache.push(result.clone());
            result
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn dp_f_0() {
        dp_f_n(0, "0");
    }
    
    #[test]
    fn dp_f_1() {
        dp_f_n(1, "100");
    }
    
    #[test]
    fn dp_f_3() {
        dp_f_n(3, "111001001100100");
    }
    
    fn dp_f_n(n: u32, expected: &str) {
        let result = dynamic_programming(n);
        assert_eq!(result.n, n);
        assert_eq!(result.content, expected);
    }
    
    #[test]
    fn dp_f_cached() {
        let result = dynamic_programming(3);
        assert_eq!(result.content, "111001001100100");
        let result = dynamic_programming_with_cache(4, result.cache);
        assert_eq!(result.content, "1111001001100100111001001100100");
    }
}
