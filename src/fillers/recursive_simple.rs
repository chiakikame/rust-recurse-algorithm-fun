use fillers::result_interface::RecursiveResult;

pub struct SimpleResult {
    n: u32,
    content: String,
    call_count: u32
}

impl RecursiveResult for SimpleResult {
    fn get_n(&self) -> u32 { self.n }
    fn get_content(&self) -> String { self.content.clone() }
    fn get_call_count(&self) -> u32 { self.call_count }
}

pub fn simple_very_naive(n: u32) -> SimpleResult {
    let mut state = Simple::new();
    let result = state.simple_very_naive(n);
    
    SimpleResult {
        n: n,
        call_count: state.call_count,
        content: result
    }
}

pub fn simple_naive(n: u32) -> SimpleResult {
    let mut state = Simple::new();
    let result = state.simple_naive(n);
    
    SimpleResult {
        n: n,
        call_count: state.call_count,
        content: result
    }
}

struct Simple {
    call_count: u32
}

impl Simple {
    fn new() -> Self {
        Simple {call_count: 0}
    }
    
    fn simple_very_naive(&mut self, n: u32) -> String {
        self.call_count += 1;
        if n == 0 {
            "0".to_string()
        } else {
            "1".to_string() + &self.simple_very_naive(n - 1) + &self.simple_very_naive(n - 1)
        }
    }

    fn simple_naive(&mut self, n: u32) -> String {
        self.call_count += 1;
        if n == 0 {
            "0".to_string()
        } else {
            let prev = self.simple_naive(n - 1);
            "1".to_string() + &prev + &prev
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn simple_vn_0() {
        simple_vn_n(0, "0");
    }
    
    #[test]
    fn simple_vn_1() {
        simple_vn_n(1, "100");
    }
    
    #[test]
    fn simple_vn_3() {
        simple_vn_n(3, "111001001100100");
    }
    
    #[test]
    fn simple_n_0() {
        simple_n_n(0, "0");
    }
    
    #[test]
    fn simple_n_1() {
        simple_n_n(1, "100");
    }
    
    #[test]
    fn simple_n_3() {
        simple_n_n(3, "111001001100100");
    }
    
    fn simple_vn_n(n: u32, expected: &str) {
        let result = simple_very_naive(n);
        assert_eq!(result.n, n);
        assert_eq!(result.content, expected);
    }
    
    fn simple_n_n(n:u32, expected: &str) {
        let result = simple_naive(n);
        assert_eq!(result.n, n);
        assert_eq!(result.content, expected);
    }
}
