use fillers::result_interface::RecursiveResult;

pub struct LoopResult {
    n: u32,
    content: String,
    call_count: u32
}

impl RecursiveResult for LoopResult {
    fn get_call_count(&self) -> u32 { self.call_count }
    fn get_content(&self) -> String { self.content.clone() }
    fn get_n(&self) -> u32 { self.n }
}

pub fn simple_loop(n: u32) -> LoopResult {
    let mut state = LoopState::new();
    let content = state.run(n);
    LoopResult {
        n: n,
        content: content,
        call_count: state.call_count
    }
}

struct LoopState {
    call_count: u32
}

impl LoopState {
    fn new() -> Self {
        LoopState { call_count: 0}
    }
    
    fn run(&mut self, n: u32) -> String {
        let mut content = "0".to_string();
        for _ in 1 .. n + 1 {
            content = "1".to_string() + &content + &content;
        }
        content
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn loop_0() {
        loop_n(0, "0");
    }
    
    #[test]
    fn loop_1() {
        loop_n(1, "100");
    }
    
    #[test]
    fn loop_3() {
        loop_n(3, "111001001100100");
    }
    
    fn loop_n(n: u32, expected: &str) {
        let result = simple_loop(n);
        assert_eq!(result.n, n);
        assert_eq!(result.content, expected);
    }
}
