pub trait RecursiveResult {
    fn get_n(&self) -> u32;
    fn get_content(&self) -> String;
    fn get_call_count(&self) -> u32;
}
